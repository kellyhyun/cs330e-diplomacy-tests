#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
#import unittest
#from unittest import main

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_create_army, diplomacy_eval, diplomacy_solve, clear_support

# -----------
# TestDiplomacy
# -----------

# replace with unittest. TestCase if did not import TestCase from unittest module
class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = "A Madrid Hold"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a, "A")
        self.assertEqual(b, "Madrid")
        self.assertEqual(c, "Hold")
        self.assertEqual(d, "")
        
    def test_read2(self):
        s = "B Barcelona Move Madrid"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a, "B")
        self.assertEqual(b, "Barcelona")
        self.assertEqual(c, "Move")
        self.assertEqual(d, "Madrid")
    
    def test_read3(self):
        s = "C London Support B"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a, "C")
        self.assertEqual(b, "London")
        self.assertEqual(c, "Support")
        self.assertEqual(d, "B")

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        name = "A"
        locationOrDead = "[dead]"
        diplomacy_print(w, name, locationOrDead)
        self.assertEqual(w.getvalue(), "A [dead]\n")
        
    def test_print2(self):
        w = StringIO()
        name = "B"
        locationOrDead = "Barcelona"
        diplomacy_print(w, name, locationOrDead)
        self.assertEqual(w.getvalue(), "B Barcelona\n")
     
    def test_print3(self):
        w = StringIO()
        name = "C"
        locationOrDead = "Madrid"
        diplomacy_print(w, name, locationOrDead)
        self.assertEqual(w.getvalue(), "C Madrid\n")

    # ----
    # create_army
    # ----
    
    def test_create1(self):
        army = diplomacy_create_army("A", "Madrid", "Hold", "")
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Madrid")
        self.assertEqual(army.action, "Hold")
        self.assertEqual(army.destination, "Madrid")

    def test_create2(self):
        army = diplomacy_create_army("A", "Madrid", "Move", "London")
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Madrid")
        self.assertEqual(army.action, "Move")
        self.assertEqual(army.destination, "London")

    def test_create3(self):
        army = diplomacy_create_army("A", "Madrid", "Support", "B")
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Madrid")
        self.assertEqual(army.action, "Support")
        self.assertEqual(army.destination, "Madrid")
        
    # ----
    # eval
    # ----
    
    def test_eval1(self):
        armyList = []
        clear_support()
        armyList.append(diplomacy_create_army("A", "Madrid", "Hold", ""))
        armyList.append(diplomacy_create_army("B", "London", "Hold", ""))
        armyList = diplomacy_eval(armyList)
        self.assertEqual(armyList[0].name, "A")
        self.assertEqual(armyList[1].name, "B")
        self.assertEqual(armyList[0].location, "Madrid")
        self.assertEqual(armyList[1].location, "London")

    def test_eval2(self):
        armyList = []
        clear_support()
        armyList.append(diplomacy_create_army("A", "Madrid", "Hold", ""))
        armyList.append(diplomacy_create_army("B", "London", "Move", "Madrid"))
        armyList = diplomacy_eval(armyList)        
        self.assertEqual(armyList[0].name, "A")
        self.assertEqual(armyList[1].name, "B")
        self.assertEqual(armyList[0].location, "[dead]")
        self.assertEqual(armyList[1].location, "[dead]")

    def test_eval3(self):
        armyList = []
        clear_support()
        armyList.append(diplomacy_create_army("A", "Madrid", "Hold", ""))
        armyList.append(diplomacy_create_army("B", "London", "Move", "Madrid"))
        armyList.append(diplomacy_create_army("C", "Barcelona", "Support", "A"))
        armyList = diplomacy_eval(armyList)
        self.assertEqual(armyList[0].name, "A")
        self.assertEqual(armyList[1].name, "B")
        self.assertEqual(armyList[2].name, "C")
        self.assertEqual(armyList[0].location, "Madrid")
        self.assertEqual(armyList[1].location, "[dead]")
        self.assertEqual(armyList[2].location, "Barcelona")

    def test_eval4(self):
        armyList = []
        clear_support()
        armyList.append(diplomacy_create_army("A", "Madrid", "Hold", ""))
        armyList.append(diplomacy_create_army("B", "London", "Move", "Madrid"))
        armyList.append(diplomacy_create_army("C", "Barcelona", "Support", "A"))
        armyList.append(diplomacy_create_army("D", "Tokyo", "Move", "Barcelona"))
        armyList = diplomacy_eval(armyList)
        self.assertEqual(armyList[0].name, "A")
        self.assertEqual(armyList[1].name, "B")
        self.assertEqual(armyList[2].name, "C")
        self.assertEqual(armyList[3].name, "D")
        self.assertEqual(armyList[0].location, "[dead]")
        self.assertEqual(armyList[1].location, "[dead]")
        self.assertEqual(armyList[2].location, "[dead]")
        self.assertEqual(armyList[3].location, "[dead]")
        
    def test_eval5(self):
        armyList = []
        clear_support()
        armyList.append(diplomacy_create_army("A", "Madrid", "Move", "London"))
        armyList.append(diplomacy_create_army("B", "London", "Move", "Madrid"))
        armyList = diplomacy_eval(armyList)
        self.assertEqual(armyList[0].name, "A")
        self.assertEqual(armyList[1].name, "B")
        self.assertEqual(armyList[0].location, "London")
        self.assertEqual(armyList[1].location, "Madrid")

    # -----
    # solve
    # -----
    def test_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve3(self):
        r = StringIO("\nA Madrid Hold\nB London Move Madrid\nC Barcelona Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC Barcelona\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC London Support B\nD Barcelona Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
