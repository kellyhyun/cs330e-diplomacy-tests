from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, assign_pts

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read1(self):
        s = StringIO("A Madrid Hold\n")
        output = diplomacy_read(s)
        answer = {'A': ('Madrid', 'Hold')}
        self.assertEqual(output,  answer)

    def test_read2(self):
        s = StringIO("A Boston Support B\nB Denver Hold\nC Atlanta Move Denver\nD Chicago Support A\nE Houston Move Boston\n")
        output = diplomacy_read(s)
        answer = {'A': ('Boston', 'Support', "B"), 'B':("Denver", "Hold"), "C":("Atlanta", "Move", "Denver"), "D": ("Chicago", 'Support', 'A'), 'E': ('Houston', 'Move', 'Boston')}
        self.assertEqual(output,  answer)

    def test_read3(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        output = diplomacy_read(s)
        answer = {'A': ('Madrid', 'Hold'), 'B':("Barcelona", "Move", "Madrid")}
        self.assertEqual(output,  answer)
    # ----
    # assign_pts
    # ----

    def testAssignPts1(self):
    	d = {'A': ('Boston', 'Support', "B"), 'B':("Denver", "Hold"), "C":("Atlanta", "Move", "Denver"), "D": ("Chicago", 'Support', 'A'), 'E': ('Houston', 'Move', 'Boston')}
    	output = assign_pts(d)
    	answer = {'A': 1, 'B': 0, 'C': 0, 'D': 0, 'E': 0}
    	self.assertEqual(output, answer)

    def testAssignPts2(self):
    	d = {'A': ('Boston', 'Support', "B"), 'B':("NewYork", "Hold"), "C":("Denver", "Support", "B"), "D": ("Austin", 'Move', 'NewYork'), 'E': ('Houston', 'Support', 'D'), 'F':('Dallas', 'Move', 'Denver')}
    	output = assign_pts(d)
    	answer = {'A': 0, 'B': 1, 'C': 0, 'D': 1, 'E': 0, 'F':0}
    	self.assertEqual(output, answer)

    def testAssignPts3(self):
    	d = {'A': ('Boston', 'Support', "B"), 'B':("Denver", "Hold"), "C":("Atlanta", "Support", "B"), "D": ("Chicago", 'Support', 'B')}
    	output = assign_pts(d)
    	answer = {'A': 0, 'B': 3, 'C': 0, 'D': 0}

    # ----
    # solve
    # ----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve7(self):
        # new test case 1
        r = StringIO("A Boston Support B\nB Denver Hold\nC Atlanta Move Denver\nD Chicago Support A\nE Houston Move Boston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Boston\nB [dead]\nC [dead]\nD Chicago\nE [dead]\n")

    def test_solve8(self):
        # new test case 2
        r = StringIO("A Madrid Move Moscow\nB Quebec Move Dallas\nC Moscow Move Quebec\nD Paris Move Madrid\nE London Move Sedona\nF Sedona Move Paris\nG Dallas Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Moscow\nB Dallas\nC Quebec\nD Madrid\nE Sedona\nF Paris\nG London\n")

    def test_solve9(self):
        # new test case 3
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC Houston Move Austin\nD Waco Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB [dead]\nC [dead]\nD Waco\n")

    def test_solve10(self):
        r = StringIO("A Austin Move Paris\nB Moscow Hold\nC Berlin Support D\nD London Move Moscow\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A Paris\nB [dead]\nC Berlin\nD Moscow\n")

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out


$ coverage report -m                   >> TestDiplomacy.out


$ cat TestCollatz.out

"""
