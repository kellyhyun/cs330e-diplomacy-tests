# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a, l, ac, t = diplomacy_read(s)
        self.assertEqual(a, "A")
        self.assertEqual(l, "Madrid")
        self.assertEqual(ac, "Hold")
        self.assertEqual(t, "")

    def test_read_2(self):
        s = "B Beijing Support D\n"
        a, l, ac, t = diplomacy_read(s)
        self.assertEqual(a, "B")
        self.assertEqual(l, "Beijing")
        self.assertEqual(ac, "Support")
        self.assertEqual(t, "D")
    
    def test_read_3(self):
        s = "C NewYork Move D\n"
        a, l, ac, t = diplomacy_read(s)
        self.assertEqual(a, "C")
        self.assertEqual(l, "NewYork")
        self.assertEqual(ac, "Move")
        self.assertEqual(t, "D")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold", ""], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Barcelona"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C Barcelona"])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Tokyo", "Hold", ""], ["B", "Beijing", "Support", "A"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "Beijing"], ["E", "NewYork", "Move", "Tokyo"]])
        self.assertEqual(v, ["A [dead]", "B Beijing", "C London", "D [dead]", "E [dead]"])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Tokyo", "Hold", ""], ["B", "Beijing", "Support", "A"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "Tokyo"], ["E", "NewYork", "Support", "D"]])
        self.assertEqual(v, ["A [dead]", "B Beijing", "C London", "D [dead]", "E NewYork"])

    # corner case: Empty Input
    def test_eval_4(self):
        v = diplomacy_eval([])
        self.assertEqual(v, [])

    # corner case: Move to city before an army is holding it
    def test_eval_5(self):
        v = diplomacy_eval([["A", "Tokyo", "Move", "Beijing"], ["B", "Beijing", "Hold", ""], ["C", "London", "Move", "Austin"], ["D", "Austin", "Support", "B"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Beijing", "C NewYork"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Beijing\nC NewYork\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]", "C [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        # general case
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")

    def test_solve_2(self):
        # corner case: attacking supports
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Beijing\nE NewYork Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE [dead]\n")
    
    def test_solve_3(self):
        # corner case: multiple levels of supports
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Tokyo\nE NewYork Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE NewYork\n")


# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
